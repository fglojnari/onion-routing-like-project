# onion-routing-like-project

<h3>Description</h3>
<br>
This project was build to understand how does the onion router works. It was for learning purposes only
so this is not something that should be used in the production.
<br>
<h3>Design</h3>
For proving the concept, we build the following system: two clients and two servers. First client is sending the message
and he is Onion Proxy (OP) and second is receiving the message. Two servers are between and they are having the role of the Onion Routers (OR).
We are sending encrypted message from 1st OP to OR, we take down first layer and send the message to the second server, where the second layer is taken and message is in plain text.


<h3>Workflow></h3>

<h5>Key exchange</h5>

    1. Client 1 (OP1) generates RSA pair
    2. Sending public key to first server(OR1)
    3. OR1 generates AES key, circleID and saves them in DB
    4. OR1 sends public key of OP1 to OR2
    5. OR2 generates AES key and circleID and saves them in DB and returns back own circleID and encrypted AES key(with public key of the OP1)
    6. OR1 saves in db circleID of OR2 as nextCircleID. Now OR1 has own AES key, own circleID and nextCircleID
    7. OR1 encrypts own AES key with public key of the OP1 and sends AES of OR1, OR2 and circleID of OR1
    8. On OP1, AES keys decrypts with private key of the OP1

<h5>Message sending</h5>

    1. OP1 encrypts the message with AES key of the OR2, then encrypted message again with OR1 AES key and sends that message to the OR1 with circleID
    2. in DB of OR1 there is circleID and for that circleID we have AES key. We decrypt the message
    3. With first layer down, we send still encrypted message to the OR2 with circleID that is saved in OR1 as nextCircleID
    4. On OR2 there is AES key for incoming circleID and message is decrypted with that key.