$( document ).ready(function() {
    $('#getKeys').click(function() {
        $.post("http://localhost:3000/negotiate", {publicKeyOfClient: publicKeyOfClient}, function(result){
            var decrypt = new JSEncrypt();
            decrypt.setPrivateKey(privateKeyOfClient);
            var uncryptedAesOne = decrypt.decrypt(result.aesKeyOne);
            var decrypt2 = new JSEncrypt();
            decrypt.setPrivateKey(privateKeyOfClient);
            var uncryptedAesTwo = decrypt.decrypt(result.aesKeyTwo);
            result.aesKeyOne = uncryptedAesOne;
            result.aesKeyTwo = uncryptedAesTwo;
            localStorage.setItem('circle', JSON.stringify(result));
        });
    });

    $('#sendMessage').click(function() {
        var circuit = JSON.parse(localStorage.getItem('circle'));
        var textToEncrypt = $("#input").val();
        var ciphertextOne = CryptoJS.AES.encrypt(textToEncrypt, circuit.aesKeyTwo);

        var ciphertextTwo = CryptoJS.AES.encrypt(ciphertextOne.toString(), circuit.aesKeyOne);

        $.post("http://localhost:3000/send", {msg: ciphertextTwo.toString(), cID: circuit.cID}, function(result){});
    });
});
var privateKeyOfClient = `-----BEGIN RSA PRIVATE KEY-----
MIIBPAIBAAJBAK99ukUvJP++kWNTOQdylMqIpqoDVCUXyF1HgLK7rfkP3WMaw2x2
uODYLbpE9qrg9MZS/oc2ZqPUJzRbXENCZwsCAwEAAQJANxTFVFW5zTOpOn2ylq3W
GcPOW+pzjLjsQKrisDbMIjHcmWSeuYNI/cZ1H2jjLIw2+snOLlsP5sbniT/SY/3b
QQIhAO5D5r8/NGrgckaweUHTFpIJ6kS3aeqtyvCM8IYuQ1KbAiEAvI2tbmiGADrl
2p6Xeof/tQ/Iaw5InEvGLd2cuLaiDFECIQDbSZkseSJoHyxtzOu83mdOf1uiZOBR
giReIokYJaoGcwIhAKrdhKWByLTaqsLDZqfeKEXu6ILDLd+c9xlHMydcMVsRAiEA
hw8g208aK3Q3AtzrSC7VrD7hMBzvyizYnKwwyLxE/Vg=
-----END RSA PRIVATE KEY-----`;

var publicKeyOfClient = `-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAK99ukUvJP++kWNTOQdylMqIpqoDVCUX
yF1HgLK7rfkP3WMaw2x2uODYLbpE9qrg9MZS/oc2ZqPUJzRbXENCZwsCAwEAAQ==
-----END PUBLIC KEY-----`;
