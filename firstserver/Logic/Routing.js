var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds135866.mlab.com:35866/niv_onion', ['circuits']);
const NodeRSA = require('node-rsa');
var request = require('request');
var CryptoJS = require("crypto-js");

class Routing {
    static createCircuit(publicKeyOfClient, cb) {
        Routing.generateRandomKey(16, function(aesKey) {
            try {
                var key = new NodeRSA({b: 512});
                key.setOptions({encryptionScheme: 'pkcs1'});
                key.importKey(publicKeyOfClient, 'public');
                key.setOptions({encryptionScheme: 'pkcs1'});
                request({
                url: "http://localhost:4000/negotiate",
                method: "POST",
                json: true,
                body: {clientPK: publicKeyOfClient}
                }, function (error, response, body) {
                    var encryptedAesKeyOfSecondServer = body.aesKey;
                    db.circuits.save({aesKey: aesKey, nextCID: body.cID}, (err, circuit) => {
                        if(circuit) {
                            cb(null, {cID: circuit._id, aesKeyOne: key.encrypt(aesKey, 'base64'), aesKeyTwo: encryptedAesKeyOfSecondServer});
                        } else {
                            cb("Couldn't create circuit!", null);
                        }
                    });
                });
            } catch(ex) {
                console.log("Exception: ", ex)
            }
        });
    }

    static generateRandomKey(length, cb) {    
        var str = "";
        for ( ; str.length < length; str += Math.random().toString( 36 ).substr( 2 ) );
        cb(str.substr( 0, length ));
    }

    static saveSecondCID(cID, nextCId, cb) {
        db.circuits.update({_id: mongojs.ObjectId(cID)}, {$set: {nextCId: nextCId}}, function(err, result) {
            if(result) {
                cb(null, true);
            } else {
                cb("Unsaved second cID", null);
            }
        });
    }

    static send(msg, cID, cb) {
        db.circuits.findOne({_id: mongojs.ObjectId(cID)}, function(err, data) {
            if(data) {
                try {
                    var aesKey = data.aesKey;
                    var decryptedBytes  = CryptoJS.AES.decrypt(msg, aesKey);
                    var decryptedMessage = decryptedBytes.toString(CryptoJS.enc.Utf8);
                    request({
                        url: "http://localhost:4000/receive",
                        method: "POST",
                        json: true,
                        body: {cID: data.nextCID, msg: decryptedMessage}
                    }, function (error, response, body){
                        if(!error) {
                            cb(null, true);
                        } else {
                            cb("Napaka pri posiljanju na naslednji streznik", null);
                        }
                    });
                } catch(ex) {
                    console.log("exception pri dekodiranju", ex);
                }
            }
        });     
    }

    /*static send(msg, cID, cb) {
        console.log("evo poruke i IDja", msg, cID)
        db.circuits.findOne({_id: mongojs.ObjectId(cID)}, function(err, data) {
            if(data) {
                try {
                    var privateKey = new NodeRSA(data.privateSKey);
                    privateKey.setOptions({encryptionScheme: 'pkcs1'});
                    console.log("evo privatnega lljuca", privateKey,"pa i date: ", data);
                    var decrypted = privateKey.decrypt(msg, 'utf8');
                    request({
                        url: "http://localhost:4000/receive",
                        method: "POST",
                        json: true,
                        body: {cID: data.nextCId, msg: decrypted}
                    }, function (error, response, body){
                        if(!error) {
                            cb(null, true);
                        } else {
                            cb("Napaka kod slanja na sljedeci server", null);
                        }
                    });
                } catch(ex) {
                    console.log("exception kod dekodiranja", ex);
                }
            }
        });     
    }*/

    static encrypt() {

    }

    static decrypt() {

    }

}
module.exports = {
    Routing
};