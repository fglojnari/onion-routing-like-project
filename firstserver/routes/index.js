var express = require('express');
var router = express.Router();
const fs = require('fs');
const NodeRSA = require('node-rsa');
let Routing = require('../Logic/Routing').Routing;
var request = require('request');
var crypto = require('crypto');

router.post('/negotiate', function(req, res, next) {
  Routing.createCircuit(req.body.publicKeyOfClient, function(err, result) {
    res.status(200).json(result);
  });
});

router.post('/send', function(req, res, next) {

  let encryptedMgs = req.body.msg;
  let cID = req.body.cID;

  Routing.send(encryptedMgs, cID, function(err, result) {
    if(result) {
      res.status(200).json({isSent: true});
    }
  });
});

module.exports = router;