const path = require('path');
const rsaWrapper = {};
const fs = require('fs');
const NodeRSA = require('node-rsa');
// open and closed keys generation method
rsaWrapper.generate = (direction) => {
    let key = new NodeRSA({b: 512});
    key.setOptions({encryptionScheme: 'pkcs1'});
    // 2048 — key length, 65537 open exponent
    //key.generateKeyPair(2048, 65537);
    //save keys as pem line in pkcs8
    fs.writeFileSync(path.resolve('keys', direction + '.private.txt'), key.exportKey('private'));
    fs.writeFileSync(path.resolve('keys', direction + '.public.txt'), key.exportKey('public'));
    return true;
};
module.exports = rsaWrapper;