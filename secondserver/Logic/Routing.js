var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds135866.mlab.com:35866/niv_onion', ['circuits2']);
var NodeRSA = require('node-rsa');
var CryptoJS = require("crypto-js");
var serverModule = require('../bin/www');
var usersConnections = serverModule.usersConnections;

class Routing {
    static createCircuit(publicKeyOfClient, cb) {
        Routing.generateRandomKey(16, function(aesKey) {
            try{
                var key = new NodeRSA({b: 512});
                key.setOptions({encryptionScheme: 'pkcs1'});
                key.importKey(publicKeyOfClient, 'public');
                key.setOptions({encryptionScheme: 'pkcs1'});
                db.circuits2.save({aesKey: aesKey}, (err, circuit) => {
                    if(circuit) {
                        cb(null, {cID: circuit._id, aesKey: key.encrypt(aesKey, 'base64')});
                    } else {
                        cb("Couldn't create circuit!", null);
                    }
                });
            } catch(ex) {
                console.log("Exception: ", ex)
            } 
        });
    }

    static generateRandomKey(length, cb) {    
        var str = "";
        for ( ; str.length < length; str += Math.random().toString( 36 ).substr( 2 ) );
        cb(str.substr( 0, length ));
    }

    static send(msg, cID, cb) {
        db.circuits2.findOne({_id: mongojs.ObjectId(cID)}, function(err, data) {
            if(data) {
                try {
                    var aesKey = data.aesKey;
                    var decryptedBytes  = CryptoJS.AES.decrypt(msg, aesKey);
                    var decryptedMessage = decryptedBytes.toString(CryptoJS.enc.Utf8); 
                    serverModule.usersConnections[0].connection.sendUTF(JSON.stringify({data: decryptedMessage}));  

                } catch(ex) {
                    console.log("exception pri dekodiranju", ex);
                }
            }
        });
    }
}
module.exports = {
    Routing
};