var express = require('express');
var router = express.Router();
let Routing = require('../Logic/Routing').Routing;
const NodeRSA = require('node-rsa');

var serverModule = require('../bin/www');


/* GET home page. */
router.post('/incomingmsg/:user_id', function(req, res, next) {
  var connection = "";
  serverModule.usersConnections.forEach(function(element) {
    if(element.id == req.params.user_id) {
      connection = element.connection;
    }
  });
  connection.sendUTF(JSON.stringify({data: req.body}));
  res.status(200).json({isSent: true})
});

router.post('/negotiate', function(req, res, next) {
  Routing.createCircuit(req.body.clientPK, function(err, result) {
    if(result) {
      res.status(200).json(result);
    } else {
      res.status(404).json(err);
    }
  });
});
router.post('/receive', function(req, res, next) {
  Routing.send(req.body.msg, req.body.cID, function(err, res) {
    if(res) {
      res.status(200).json({isSent: true});
    } else {
      res.status(404).json(err);
    }
  });
});

module.exports = router;
